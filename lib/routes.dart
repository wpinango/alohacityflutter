
import 'package:aloha_city/screens/account/account_screen.dart';
import 'package:aloha_city/screens/business/business_screen.dart';
import 'package:aloha_city/screens/contact/contact_screen.dart';
import 'package:aloha_city/screens/login/login_screen.dart';
import 'package:aloha_city/screens/main/order_screen.dart';
import 'package:aloha_city/screens/new_order_screen/new_order_screen.dart';
import 'package:aloha_city/screens/pass_recovery/password_recovery_screen.dart';
import 'package:aloha_city/screens/splash/splash_screen.dart';
import 'package:aloha_city/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  var routes = <String, WidgetBuilder> {
    "/Login": (BuildContext context) => new LoginScreen(),
    "/Orders": (BuildContext context) => new OrderScreen(),
    "/NewOrder":(BuildContext context) => new NewOrderScreen(),
    "/Business":(BuildContext context) => new BusinessScreen(),
    "/Account":(BuildContext context) => new AccountScreen(),
    "/Contact":(BuildContext context) => new ContactScreen(),
    "/PasswordRecovery": (BuildContext context) => new PasswordRecoveryScreen()
  };

  Routes() {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Aloha City",
      home: new FirstScreen(),
      routes: routes,
      theme: ThemeData(
        primaryColor: primaryColor,
      ),
    ));
  }

  static replacementScreen(BuildContext context, String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  static showModalScreen(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }
}