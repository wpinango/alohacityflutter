

import 'package:intl/intl.dart';

import 'models/country.dart';

class Global{
  static String keyCountry = "country";
  static String keySelectedCountry = 'selectedCountry';
  static String keyCredentials = 'credentials';
  static String keyLogin = 'login';
  static String keyApiUrl = 'api_url';
  static String appName = 'Aloha24';
  static String appVersion = 'v020300';
  static String platform = 'ios';

  static NumberFormat getCurrencySymbol(Country selectedCountry) {
    var f;
    if (selectedCountry.completeCode == 'es-ec') {
      f = new NumberFormat.simpleCurrency(locale: 'es_US',name: 'PEN');
    } else if (selectedCountry.completeCode == 'es-es') {
      f = new NumberFormat.simpleCurrency(locale: 'es-ES');
    }
    return f;
  }
}