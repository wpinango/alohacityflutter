class Credentials {
  String user;
  String password;

  Credentials({this.password, this.user});

  toJson() {
    return{
      'user': user,
      'password':password,
    };
  }

  factory Credentials.fromJson(Map<String, dynamic> json) {
   return Credentials(
     password: json['password'],
     user: json['user']
   );
  }
}