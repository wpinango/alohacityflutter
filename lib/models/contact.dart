class Contact {
  String address;
  String latitude;
  String longitude;
  String phone;
  String email;
  String fax;
  String web;
  String schedule;

  Contact(
      {this.address,
      this.phone,
      this.email,
      this.longitude,
      this.latitude,
      this.schedule,
      this.fax,
      this.web});

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
      address: json['direccion'],
      latitude: json['latitud'],
      longitude: json['longitude'],
      phone: json['telefono'],
      email: json['email'],
      fax: json['fax'],
      web: json['web'],
      schedule: json['horario']
    );
  }

  Map<String, dynamic> toJson() => {
    'direccion': address,
    'latitud': latitude,
    'longitude':longitude,
    'telefono': phone,
    'email':email,
    'fax':fax,
    'web':web,
    'schedule': schedule,
  };
}

