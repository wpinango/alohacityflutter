class Masters {
  String ts;
  List faq;
  List contact;

  Masters({this.contact, this.faq, this.ts});

  factory Masters.fromJson(Map<String, dynamic> json) {
    return Masters(
        ts: json['ts'],
        faq: json['faq'],
        contact: json['contact'],
    );
  }

  Map<String, dynamic> toJson() => {
    'ts': ts,
    'faq': faq,
    'contact':contact,
  };
}