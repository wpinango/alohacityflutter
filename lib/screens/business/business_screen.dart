import 'dart:convert';

import 'package:aloha_city/models/business.dart';
import 'package:aloha_city/models/login_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../theme.dart';

class BusinessScreen extends StatefulWidget {
  const BusinessScreen({Key key}) : super(key: key);

  @override
  BusinessScreenState createState() => new BusinessScreenState();
}

class BusinessScreenState extends State<BusinessScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Business business;
  List<Business> businesses = new List();
  LoginResponse loginResponse;

  @override
  void initState() {
    super.initState();
    getBusiness();
  }

  void getBusiness() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        for (var a in loginResponse.business) {
          Business business = new Business();
          business.name = a['nombre'];
          business.info = a['info'];
          business.address = a['address'];
          business.addressId = a['id_direccion'];
          business.social = a['razon_social'];
          business.nif = a['nif'];
          business.phoneMobile = a['movil'];
          business.phone = a['telefono'];
          business.businessId = a['id_negocio'];
          businesses.add(business);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "Aloha24",
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: new Container(
          height: screenSize.height,
          width: screenSize.width,
          padding: new EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Negocios',
                    style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 22),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
              ),
              Container(
                height: screenSize.height / 1.5,
                width: screenSize.width,
                child: new ListView.builder(
                  itemCount: businesses.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.favorite),
                            padding: EdgeInsets.all(16),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                businesses[index].name,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              businesses[index].social != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Razon social: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          child: Text(businesses[index].social),
                                          width: screenSize.width / 2.3,
                                        ),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              businesses[index].nif != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Nif/Cif: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(businesses[index].nif),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              businesses[index].address != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Direccion: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          child:
                                              Text(businesses[index].address),
                                          width: screenSize.width / 2,
                                        ),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              businesses[index].info != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Info direccion: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(businesses[index].info),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              businesses[index].phone != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Telefono: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(businesses[index].phone),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              businesses[index].phoneMobile != null
                                  ? Row(
                                      children: <Widget>[
                                        Text(
                                          'Movil: ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(businesses[index].phoneMobile),
                                      ]
                                          .where((child) => child != null)
                                          .toList(),
                                    )
                                  : null,
                              new Divider(color: Colors.grey)
                            ].where((child) => child != null).toList(),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              )
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }
}
