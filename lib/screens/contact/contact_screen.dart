import 'dart:convert';

import 'package:aloha_city/components/TextFields/input_field.dart';
import 'package:aloha_city/components/buttons/rounded_button.dart';
import 'package:aloha_city/dialogs/dialog.dart';
import 'package:aloha_city/models/contact.dart';
import 'package:aloha_city/models/login_response.dart';
import 'package:aloha_city/models/masters.dart';
import 'package:aloha_city/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../theme.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key key}) : super(key: key);

  @override
  ContactScreenState createState() => new ContactScreenState();
}

class ContactScreenState extends State<ContactScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Validations validations = new Validations();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 30);
  LoginResponse loginResponse;
  Masters masters;
  List<Contact> contacts = new List();

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        masters =
            Masters.fromJson(json.decode(json.encode(loginResponse.masters)));
        for (var a in masters.contact) {
          Contact contact = new Contact();
          contact.schedule = a['horario'];
          contact.web = a['web'];
          contact.fax = a['fax'];
          contact.email = a['email'];
          contact.phone = a['phone'];
          contact.longitude = a['longitude'];
          contact.latitude = a['latitude'];
          contact.address = a['direccion'];
          contacts.add(contact);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "Aloha24",
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Conctata con nosotros',
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22),
                    ),
                  ],
                ),
                Container(
                  padding: new EdgeInsets.all(7.0),
                  child: Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Nombre',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        new InputField(
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              //user.name = value;
                            }),
                        new Text(
                          'Email',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        new InputField(
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              //user.lastName = value;
                            }),
                        new Text(
                          'Mensaje',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        new InputField(
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              //user.email = value;
                            }),
                        new RoundedButton(
                          buttonName: "Enviar",
                          onTap: handleFormSubmitted,
                          width: screenSize.width,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: primaryColor,
                        ),
                      ].where((child) => child != null).toList(),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                new Text(
                  'Donde estamos?',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.location_on),
                    Flexible(
                      child: Text(contacts[0].address),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.phone),
                    //Flexible(child: Row(children: <Widget>[],),),
                    Flexible(
                      child: Text(contacts[0].phone != null
                          ? contacts[0].phone
                          : contacts[0].fax),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.email),
                    Flexible(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(contacts[0].email),
                        Text(contacts[0].web)
                      ],
                    )),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                new Text(
                  'Horario',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.schedule),
                    Flexible(
                      child: Text(contacts[0].schedule),
                    ),
                  ],
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      showMessageDialog();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void showMessageDialog() {
    MyDialog.showNativePopUpWith2Buttons(
        context,
        'Informacion',
        'Ha ocurrido un problema y no se puede enviar el mensaje',
        'Cancelar',
        'Aceptar');
  }
}
