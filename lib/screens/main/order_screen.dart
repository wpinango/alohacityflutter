import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:aloha_city/database/database_helper.dart';
import 'package:aloha_city/dialogs/dialog.dart';
import 'package:aloha_city/global.dart';
import 'package:aloha_city/models/business_delivery.dart';
import 'package:aloha_city/models/country.dart';
import 'package:aloha_city/models/delivery.dart';
import 'package:aloha_city/models/login_response.dart';
import 'package:aloha_city/screens/new_order_screen/new_order_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_url.dart';
import '../../routes.dart';
import '../../theme.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key key}) : super(key: key);

  @override
  OrderScreenState createState() => new OrderScreenState();
}

class OrderScreenState extends State<OrderScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> values = ['Pedidos activos', 'Pedidos pendientes'];
  bool isInAsyncCall = false;
  String country = 'Pedidos activos';
  final dbHelper = DatabaseHelper.instance;
  List businessOrders = new List();
  List businessDeliveries = new List();
  bool isActive = true;
  var timeout = const Duration(seconds: 10);
  Country selectedCountry;
  LoginResponse loginResponse;
  List businessDeliveriesTemp = new List();
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  StreamSubscription iosSubscription;
  BusinessDelivery businessDelivery;

  @override
  initState() {
    super.initState();
    initVars();
    getSelectedCountry();
    if (Platform.isIOS) {
      iosSubscription =
          firebaseMessaging.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      firebaseMessaging
          .requestNotificationPermissions(IosNotificationSettings());
    }
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    loginResponse = LoginResponse.fromJson(json.decode(prefs.get('loginData')));
  }

  initVars() async {
    businessOrders = new List();
    businessDeliveries = new List();
    businessDeliveriesTemp = new List();
    businessDelivery = new BusinessDelivery();
    businessDelivery.deliveries = new Map();
    final allRows = await dbHelper.queryAllRows();
    //allRows.forEach((row) => print(row));
    /*dbHelper.queryRowCount().then((value) {
      print(value);
    });*/
    allRows
        .forEach((row) => businessDeliveriesTemp.add((Delivery.fromJson(row))));
    setState(() {});
    refreshData();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void refreshData() async {
    showProgressDialog();
    try {
      List references = new List();
      for (var a in businessDeliveriesTemp) {
        references.add(a.reference);
      }
      var body = {
        'deliveries': references,
        'token': loginResponse.token,
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
              selectedCountry.protocol +
                  '://' +
                  selectedCountry.url +
                  ApiUrl.urlGetOrders.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleRefreshData(response.body);
      } else {
        handleRefreshData("");
      }
    } catch (e) {
      handleRefreshData("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleRefreshData(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        try {
          for (var a in map['resultado']['deliveries']) {
            for (var b in businessDeliveriesTemp) {
              if (a['referencia'] == b.reference) {
                if (a['estado'] == 1) {
                  businessDeliveries.add(b);
                } else {
                  //businessOrders.add(b);
                }
              }
            }
          }
          businessDelivery.deliveries = new Map();
          List<Delivery> delivery = new List();
          for (var a in businessDeliveries) {
            if (businessDelivery.deliveries.containsKey(a.business)) {
              delivery.add(a);
              businessDelivery.deliveries[a.business] = delivery;
            } else {
              delivery = new List();
              delivery.add(a);
              businessDelivery.deliveries[a.business] = delivery;
            }
          }
          setState(() {});
        } catch (e) {
          print(e.toString());
        }
      } else {
        showInSnackBar('No se ha podido obtener el listado de pedidos.');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "Aloha24",
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: new Container(
          height: screenSize.height,
          width: screenSize.width,
          padding: new EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Mis pedidos',
                    style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 22),
                  ),
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () {},
                  ),
                ],
              ),
              Theme(
                  data: new ThemeData(
                      canvasColor: Colors.black87,
                      primaryColor: Colors.black,
                      accentColor: Colors.black,
                      hintColor: Colors.black),
                  child: Container(
                      height: screenSize.height / 13,
                      width: screenSize.width / 1.05,
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      decoration: BoxDecoration(
                        color: Colors.black87,
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                            color: white,
                            style: BorderStyle.solid,
                            width: 0.80),
                      ),
                      child: new DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                            items: values
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: TextStyle(color: white),
                                ),
                              );
                            }).toList(),
                            onChanged: (String value) {
                              setState(() {
                                country = value;
                                isActive = !isActive;
                              });
                            },
                            value: country),
                      ))),
              isActive
                  ? Container(
                      height: screenSize.height / 1.5,
                      child: new ListView.builder(
                        itemCount: businessOrders.length,
                        itemBuilder: (BuildContext context, int index) {
                          Uint8List decoded;
                          if (businessOrders.length > 0) {
                            decoded = base64Decode(businessOrders[index]
                                .ticket
                                .split('data:image/jpeg;base64,')[1]);
                          }
                          return new Container(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(238, 238, 238, 1)),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      child: Text('Ref.' +
                                          businessOrders[index].reference),
                                      padding: EdgeInsets.all(16),
                                      alignment: Alignment.centerLeft),
                                  Container(
                                    decoration: BoxDecoration(
                                        color:
                                            Color.fromRGBO(251, 222, 204, 1)),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          child: Text('Observaciones: ' +
                                              businessOrders[index]
                                                  .observations),
                                          padding: EdgeInsets.all(16),
                                          alignment: Alignment.centerLeft,
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          child: Image.memory(decoded),
                                          width: screenSize.width,
                                          height: screenSize.height / 2.5,
                                          padding: EdgeInsets.all(8),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : Container(
                      height: screenSize.height / 1.5,
                      child: new ListView.builder(
                        itemCount: businessDelivery.deliveries.length != null
                            ? businessDelivery.deliveries.length
                            : 0,
                        itemBuilder: (context, i) {
                          var list = businessDelivery.deliveries.keys.toList();
                          return Container(
                            child: new ExpansionTile(
                              backgroundColor: white,
                              title: new Text(
                                list[i],
                                style: new TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              children: <Widget>[
                                new Column(
                                  children: buildExpandableContent(
                                      businessDelivery, screenSize),
                                ),
                              ].where((child) => child != null).toList(),
                            ),
                          );
                        },
                      ),
                    ),
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      drawer: new Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.black87,
          ),
          child: Container(
            width: screenSize.width / 1.5,
            child: new Drawer(
              child: new ListView(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(
                        Icons.send,
                        color: white,
                      ),
                      title: new Text(
                        'Enviar pedido',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        //Routes.showModalScreen(context, '/NewOrder');
                        //onLoginButtonPressed();
                        openNewOrderScreen();
                      }),
                  /*new ListTile(
                      leading: new Icon(
                        Icons.business_center,
                        color: white,
                      ),
                      title: new Text(
                        'Historial',
                        style: new TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        //onPressed(context, '/SignUp');
                      }),*/
                  new ListTile(
                      leading: new Icon(
                        Icons.account_balance,
                        color: white,
                      ),
                      title: new Text(
                        'Negocios',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Routes.showModalScreen(context, '/Business');
                      }),
                  new ListTile(
                      leading: new Icon(
                        Icons.person,
                        color: white,
                      ),
                      title: new Text(
                        'Mi cuenta',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Routes.showModalScreen(context, '/Account');
                      }),
                  new ListTile(
                      leading: new Icon(
                        Icons.phone,
                        color: white,
                      ),
                      title: new Text(
                        'Contacto',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Routes.showModalScreen(context, '/Contact');
                      }),
                  /*new ListTile(
                      leading: new Icon(
                        Icons.question_answer,
                        color: white,
                      ),
                      title: new Text(
                        'FAQ',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        //openCountryScreen();
                      }),*/
                  new ListTile(
                      leading: new Icon(
                        Icons.exit_to_app,
                        color: white,
                      ),
                      title: new Text(
                        'Cerrar sesion',
                        style: TextStyle(color: white),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        closeSessionDialog();
                        //openCountryScreen();
                      }),
                ],
              ),
            ),
          )),
      resizeToAvoidBottomPadding: false,
    );
  }

  buildExpandableContent(BusinessDelivery businessDelivery, Size screenSize) {
    List<Widget> columnContent = [];
    for (var a in businessDelivery.deliveries.entries) {
      for (Delivery d in a.value) {
        Uint8List decoded =
            base64Decode(d.ticket.split('data:image/jpeg;base64,')[1]);
        columnContent.add(
          Container(
            decoration: BoxDecoration(color: Color.fromRGBO(238, 238, 238, 1)),
            child: Column(
              children: <Widget>[
                Container(
                    child: Text('Ref.' + d.reference),
                    padding: EdgeInsets.all(16),
                    alignment: Alignment.centerLeft),
                Container(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(251, 222, 204, 1)),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text('Observaciones: ' + d.observations),
                        padding: EdgeInsets.all(16),
                        alignment: Alignment.centerLeft,
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Image.memory(decoded),
                        width: screenSize.width,
                        height: screenSize.height / 2.5,
                        padding: EdgeInsets.all(8),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }
    return columnContent;
  }

  void openNewOrderScreen() async {
    if (await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new NewOrderScreen()))) {
      initVars();
    }
  }

  void closeSessionDialog() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context, "Alerta", 'Desear cerrar sesion?', 'Cancelar', "Aceptar")) {
      doLogout();
    }
  }

  void doLogout() async {
    showProgressDialog();
    try {
      var body = {
        'token': loginResponse.token,
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
              selectedCountry.protocol +
                  '://' +
                  selectedCountry.url +
                  ApiUrl.urlLogout.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleLogoutResponse(response.body);
      } else {
        handleLogoutResponse("");
      }
    } catch (e) {
      handleLogoutResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleLogoutResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        try {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString(Global.keyCredentials, json.encode(''));
          prefs.setString('loginData', json.encode(''));
          prefs.setBool('login', false);
          for (var a in businessDeliveriesTemp) {
            dbHelper.delete(a.reference);
          }
          Routes.replacementScreen(context, '/Login');
          setState(() {});
        } catch (e) {
          print(e.toString());
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }
}
