import 'dart:convert';

import 'package:aloha_city/components/TextFields/input_field.dart';
import 'package:aloha_city/components/TextFields/password_input_field.dart';
import 'package:aloha_city/components/buttons/rounded_button.dart';
import 'package:aloha_city/dialogs/dialog.dart';
import 'package:aloha_city/models/country.dart';
import 'package:aloha_city/models/credentials.dart';
import 'package:aloha_city/models/login_response.dart';
import 'package:aloha_city/models/user.dart';
import 'package:aloha_city/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key key}) : super(key: key);

  @override
  AccountScreenState createState() => new AccountScreenState();
}

class AccountScreenState extends State<AccountScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Validations validations = new Validations();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 30);
  List<Country> countryList = new List();
  List<String> values = ['España'];
  String country;
  Country selectedCountry;
  LoginResponse loginResponse;
  TextEditingController nameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController infoController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  User user;
  String password;

  @override
  void initState() {
    super.initState();
    getUserData();
    getSelectedCountry();
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
        country = selectedCountry.name;
      });
    }
    if (selectedCountry.name == null) {
      requestVersion();
    }
  }

  void getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        print(loginResponse.user['nombre']);
        nameController.text = loginResponse.user['nombre'];
        lastNameController.text = loginResponse.user['apellidos'];
        emailController.text = loginResponse.user['email'];
        phoneController.text = loginResponse.user['telefono'];
        mobileController.text = loginResponse.user['movil'];
        addressController.text = loginResponse.user['address'];
        infoController.text = loginResponse.user['info'];
      });
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestVersion() async {
    showProgressDialog();
    try {
      var body = {'version': 'v020405', 'platform': 'android', 'token': 0};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(ApiUrl.urlPrefix + ApiUrl.urlGetVersion,
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleVersionResponse(response.body);
      } else {
        handleVersionResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleVersionResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      for (var item in map['resultado']['businesses']) {
        Country country = new Country();
        country.name = item[0].toString().replaceAll('Aloha', 'España');
        country.url = item[1];
        country.code = item[3].toString().split('-')[1];
        country.completeCode = item[3];
        country.protocol = item[6];
        countryList.add(country);
        if (!values.contains(country.name)) {
          values.add(country.name);
        }
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  void updateProfile() async {
    showProgressDialog();
    try {
      var body = {
        'email': user.email,
        'passowrd': password,
        'nombre': user.name,
        'apellidos': user.lastName,
        'telefono': user.phone,
        'movil': user.mobileNumber,
        'address': user.address,
        'info': user.info
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(ApiUrl.urlPrefix + ApiUrl.urlGetVersion,
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleUpdateUserResponse(response.body);
      } else {
        handleUpdateUserResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleUpdateUserResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        onSelectedCountry();
        Credentials credentials = new Credentials();
        credentials.password = password;
        credentials.user = user.email;
        final prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setString(Global.keyCredentials, json.encode(credentials));
          //prefs.setString('loginData', json.encode(loginResponse));
          //prefs.setBool('login', true);
        });
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "Aloha24",
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            height: screenSize.height * 1.7,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Mis datos',
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Container(
                  child: Image.asset('assets/ico-perfil.png'),
                ),
                Theme(
                    data: new ThemeData(
                        canvasColor: Colors.black87,
                        primaryColor: Colors.black,
                        accentColor: Colors.black,
                        hintColor: Colors.black),
                    child: Container(
                        margin: new EdgeInsets.only(bottom: 12.0, top: 12.0),
                        height: screenSize.height / 13,
                        width: screenSize.width / 1.1,
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          color: Colors.black87,
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                              color: white,
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: new DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              hint: Text(
                                'Seleccione un pais',
                                style: TextStyle(color: white),
                              ),
                              items: values.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: country,
                                  child: Text(
                                    country,
                                    style: TextStyle(color: white),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  country = value;
                                });
                              },
                              value: country),
                        ))),
                Container(
                  padding: new EdgeInsets.all(7.0),
                  child: Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      children: <Widget>[
                        new InputField(
                            hintText: 'Nombre',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: nameController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.name = value;
                            }),
                        new InputField(
                            hintText: 'Apellidos',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: lastNameController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.lastName = value;
                            }),
                        new InputField(
                            hintText: 'Email',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: emailController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.email = value;
                            }),
                        new InputField(
                            hintText: 'Telefono',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: phoneController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            //validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.phone = value;
                            }),
                        new InputField(
                            hintText: 'Telefono movil',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: mobileController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            //validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.mobileNumber = value;
                            }),
                        new InputField(
                            hintText: 'Direccion',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: addressController,
                            enable: false,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.address = value;
                            }),
                        new InputField(
                            hintText: 'Info',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            controller: infoController,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              user.info = value;
                            }),
                        Padding(padding: EdgeInsets.all(5)),
                        Text(
                          'Cambio de contraseña',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        new PasswordInputField(
                          hintText: "Contraseña",
                          obscureText: true,
                          //controller: passwordController,
                          textInputType: TextInputType.text,
                          textStyle: textStyle,
                          hintStyle: hintStyle,
                          textFieldColor: textFieldColor,
                          bottomMargin: 10.0,
                          //validateFunction: validations.validatePassword,
                          onSaved: (String password) {

                          },
                        ),
                        new PasswordInputField(
                          hintText: "Contraseña nueva",
                          obscureText: true,
                          controller: passwordController,
                          textInputType: TextInputType.text,
                          textStyle: textStyle,
                          hintStyle: hintStyle,
                          textFieldColor: textFieldColor,
                          bottomMargin: 10.0,
                          validateFunction: validations.validatePassword,
                          onSaved: (String password) {
                            this.password = password;
                          },
                        ),
                        new PasswordInputField(
                          hintText: "Repita contraseña nueva",
                          obscureText: true,
                          textInputType: TextInputType.text,
                          textStyle: textStyle,
                          hintStyle: hintStyle,
                          textFieldColor: textFieldColor,
                          bottomMargin: 10.0,
                          validateFunction: (value) =>
                              validations.validateRepeatPassword(
                                  value, passwordController.text),
                          onSaved: (String password) {},
                        ),
                        new RoundedButton(
                          buttonName: "Guardar",
                          onTap: handleFormSubmitted,
                          width: screenSize.width,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: primaryColor,
                        ),
                      ].where((child) => child != null).toList(),
                    ),
                  ),
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      showMessageDialog();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void showMessageDialog() {
    MyDialog.showNativePopUpWith2Buttons(
        context,
        'Informacion',
        'Ha ocurrido un problema y no se puede enviar el mensaje',
        'Cancelar',
        'Aceptar');
  }

  onSelectedCountry() async {
    if (country != null) {
      for (var c in countryList) {
        if (c.name == country) {
          setState(() {
            selectedCountry = c;
          });
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('selectedCountry', json.encode(selectedCountry));
          prefs.setString(Global.keyApiUrl,
              selectedCountry.protocol + '://' + selectedCountry.url);
        }
      }
    } else {
      showInSnackBar("Debe seleccionar un pais");
    }
  }
}
