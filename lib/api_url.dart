/*
* const URL_LOGIN = "/?/ws_getAuth.php";
const URL_LOGOUT = "/?/ws_setLogout.php";
const URL_PERFIL = "/?/ws_getProfile.php";
const URL_SET_PROFILE = "/?/ws_setProfile.php";
const URL_SET_PASSWORD = "/?/ws_getPassword.php";
const URL_SET_MAIL = "/?/ws_setMail.php";
const URL_GET_MASTERS = "/?/ws_getMasters.php";
const URL_SET_DELIVERY_TICKET = "/?/ws_setTicket.php";
const URL_SET_AVATAR = "/?/ws_setAvatar.php";
const URL_GET_ORDERS = "/?/ws_getOrders.php";
const URL_SET_PUSH_REGISTATION_ID = "/?/ws_setPushService.php";
const URL_GET_VERSION = "/ws_getVersion.php";
const URL_SET_ESTADO_PEDIDO = "/?/ws_setOrder.php";*/
//const URL_PREFIX = "http://aloha24.com/alohages/ws/city";
//const VERSION_APP = "v020405";

class ApiUrl {
  static final String urlPrefix = 'http://aloha24.com/alohages/ws/city';
  static final String urlLogin = '/?/ws_getAuth.php';
  static final String urlGetVersion = '/ws_getVersion.php';
  static final String urlSetTicket = '/?/ws_setTicket.php';
  static final String urlGetOrders = '/?/ws_getOrders.php';
  static final String urlLogout = "/?/ws_setLogout.php";
  static final String urlGetProfile = '/?/ws_getProfile.php';
}